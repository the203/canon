import re
from collections import UserDict
from datetime import datetime as Datetime
from logging import warning


class ConverterBundle(UserDict):

    def register(self, k, v):
        self[k] = v

    @staticmethod
    def parse_datetime(v):
        # python iso format doesn't like micro seconds
        if v[-1] == "Z":
            v = v[:-1] + "-00:00:00"
        dt, micros = (v.split(".") + [None])[0:2]
        post_fix = ""
        if micros is not None:
            tz_pos = (micros.find('-') + 1 or micros.find('+') +1) -1
            post_fix = f".{(micros[:tz_pos] + ('0' *6))[:6]}{micros[tz_pos:]}"

        return Datetime.fromisoformat(dt +post_fix)


convert_bundle = ConverterBundle()

convert_bundle[Datetime] =ConverterBundle.parse_datetime
