import inspect
import logging
from dataclasses import dataclass, field, InitVar
from functools import partial
from typing import Any, Text, Callable, List, Union

from datamapping._helpers import ascls
from datamapping.converters import convert_bundle
from datamapping.mappable import MappingInfo

logger = logging.getLogger("datamapping.field")

__all__ = [
    'map_to',
    'Ignore',
    'FieldMapping',
    'MapTo',
    'Preserve',
    'TBD'
]


def map_to(field=None, converter=lambda value, key: value):
    return FieldMapping(field, converter=converter)


class DeferredInstanceCall(object):
    """Wrapping a converter with this allows the coonverter to be called


    """

    @property
    def __name__(self):
        return self._deferred

    def __init__(self, deferred):
        if callable(deferred):
            self._deferred = deferred.__name__
        else:
            self._deferred = deferred
        self._instance = None
        self._callable = None

    def __call__(self, *args, **kwargs):
        return self._callable(*args, **kwargs)

    def dereference(self, instance=None):
        if self._callable is None and instance is not None:
            self._callable = getattr(instance, self._deferred)
        return self._callable


@dataclass
class FieldMapping(object):
    target: Union[Callable[[Any, Any], None], Text, Any] = field(default=None)
    converter: Callable = field(default=None)
    path: Text = field(default=None)
    context: object = field(default=None)
    target_kwargs: InitVar[dict] = field(default=None)
    init_data: bool = field(default=False)
    _path_split: Text = field(init=False, default=None)
    _tokenized_path: List[Text] = field(init=False, default=None)
    _name: Text = field(init=False, default="")

    @property
    def mapper(self):
        try:
            return self._mapper
        except AttributeError:
            self._mapper = None
            return None

    @mapper.setter
    def mapper(self, v):
        self._mapper = v

    @property
    def name(self):
        return self._name or ascls(self.converter).__name__

    @property
    def key(self):
        return self._name

    def __post_init__(self, target_kwargs=None):
        from datamapping import SourceMapping

        if isinstance(self.target, type) and issubclass(self.target, SourceMapping):
            self.converter = self.target
            self.target = None

        converter = self.converter
        # special case a to support a cleaner interface for embedded mappings.
        if isinstance(self.converter, type) and issubclass(self.converter, SourceMapping):
            self.converter: SourceMapping = self.converter(root=self.path)
            converter = self.converter.map_item
        elif isinstance(self.target, MappingInfo) and self.converter is None:
            self.converter = converter = convert_bundle.get(self.target.type, None)

        if converter is not None and not isinstance(converter, DeferredInstanceCall):
            self._configure_converter_args(converter)
        if self.target:
            self.configure_target(target_kwargs=target_kwargs)

    def configure_target(self, force=False, target_kwargs=None):
        if target_kwargs is not None:
            self._name = self.target.__name__
            self.target = partial(self.target, **target_kwargs)
        try:
            self.context = self.target.owner or self.context
        except AttributeError as ex:
            pass

        if callable(self.target) and not force:
            try:
                self._name = self.target.__name__
            except AttributeError:
                ...
            return

        target = self.target
        try:
            target = target.name
        except Exception:
            try:
                target = target.fset.__name__
            except AttributeError:
                pass

        if isinstance(target, str):
            self.target = target
            self._name = self.target
        else:
            path = self.path
            addendum = ""
            if path is None and target is None:
                calframe = inspect.getouterframes(inspect.currentframe(), 2)[3]
                code = calframe.code_context[-1]
                d = code.split("=")
                path = d[0].lstrip().rstrip()
                target = "=".join(d[1:]).split(",")[0].split("(")[1].lstrip().rstrip()
                obj = target.split('.')[0].lstrip().rstrip()
                addendum = f"Check and make sure {obj} is mappable. " \
                           f"Usually this happens when @maps(to={obj}) is used"
            msg = f"'{path}' could not be mapped. {target} is not callable. {addendum}"
            raise AttributeError(msg)

    @property
    def tokenized_path(self):
        if self._tokenized_path is None or self.path != self._path_split:
            self._tokenized_path = self.path.split(".")
            self._path_split = self.path
        return self._tokenized_path

    def extract_value(self, raw_data, path_pos=1):
        value = raw_data
        for node in self.tokenized_path[path_pos:]:
            try:
                value = value[node]
            except KeyError:
                logger.warning(f"Node '{node}' not found. Nodes found: {','.join(value.keys())}")
            except TypeError:
                if isinstance(value, list):
                    value = value[int(node)]
                else:
                    logger.error(f"{value} can not be sliced by {node}")
                    raise
        return value

    def call_target(self, item, value):
        # value = self.convert(value)
        if not self.target:
            logger.debug(f"No target for {self.name}, {value} lost")
            return False
        if not callable(self.target):
            setattr(item, self.target, value)
            return True
        try:
            self.target(item, value)
        except TypeError:
            self.target(value)

        return True

    def convert(self, value):
        if self.converter:
            if isinstance(self.converter, DeferredInstanceCall):
                self.converter = self.converter.dereference()
                self._configure_converter_args(self.converter)
            kwargs = {self.converter_arg_map["value"]: value}
            if self.converter_arg_map["key"]:
                kwargs[self.converter_arg_map["key"]] = self.path
            if self.converter_arg_map["mapping"]:
                kwargs[self.converter_arg_map["mapping"]] = self.mapper
            converter = self.converter
            from datamapping import SourceMapping
            if isinstance(converter, SourceMapping):
                if converter.path is None:
                    converter.path = self.path
                converter = converter.map_item
            try:
                    value = converter(**kwargs)
            except TypeError:
                value = converter(*kwargs.values())
        return value

    def _configure_converter_args(self, converter):
        converter_args = inspect.signature(converter).parameters
        if list(converter_args)[0] == 'self':
            msg = f"Bad Converter: {converter}\n" \
                  "Methods intended to be instance bound can not be converters. If the converter needs a " \
                  "reference to the mapping the converter should have a mapping argument as partt of its signature"
            raise ValueError(msg)
        self.converter_arg_map = {"value": None, "key": None, "mapping": None}
        if "mapping" in converter_args:
            self.converter_arg_map['mapping'] = "mapping"
        if "value" in converter_args:
            self.converter_arg_map['value'] = "value"
        if "key" in converter_args:
            self.converter_arg_map["key"] = "key"
        if self.converter_arg_map["value"] is None:
            converter_args = list(converter_args.values())
            value = converter_args.pop(0)
            self.converter_arg_map["value"] = value.name


@dataclass
class DebugFieldMapping(FieldMapping):
    """Often times when debugging mapings the breakpoints needs to be in some common/shared code. This usually results
     in either skipping over a break ponit many times, or finding a breakpoint above the call and tidiously stepping
     over and into lines. Enter DebugFieldMapping. Flip out the FieldMapping to DebugFieldMapping and add a conditional
     breakpoint type(fm).__name__ == "DebugFieldMapping".

    """
    pass


@dataclass
class Preserve(FieldMapping):



    def __set_name__(self, owner, name):
        if self.path is None:
            self.path = name
        if self.target is None:
            self.target = self.path
            self.configure_target()


@dataclass
class Ignore(FieldMapping):
    target: Callable = field(default=lambda: "")

    def update_item(self, item, value):
        return None


Pass = Ignore


@dataclass
class EmbeddedValues(FieldMapping):
    """There are instances where there might be embedded dictionaries but the resulting mapping
    may not follow the same structure. When the values are important but the structure is not a EmbeddedValues
    can be used to map the deeper part of the structure. Ignore could be used but the syntax (and the name) imply that
    the referenced sub tree is entirely ignored. EmbeddedValues is a special case of Ignore that will effectively
    take a mapping (or any sort of value converter) and run the conversion but the output will be ignored.

    """

    def __post_init__(self, target_kwargs):
        """

        :return:
        """
        self.converter = self.target
        self.target = lambda: None
        super().__post_init__(target_kwargs)


class MapTo(Preserve): ...


class TBD(Preserve):
    pass


class Unknown(Preserve):
    pass
