class _Empty(object): ...


def ascls(obj):
    if isinstance(obj, type):
        return obj
    else:
        return type(obj)