import json
import logging
from dataclasses import field, dataclass, is_dataclass, fields
from datetime import datetime as DateTime
from functools import wraps
from typing import List, Type, TypeVar, Text, Any, get_origin, get_args, Callable, Union

from datamapping.exceptions import MappingError
from datamapping.mappable import mappable
from ._helpers import _Empty, ascls
from ._helpers.generics import is_generic_type, get_bound, get_parameters, get_generic_type
from .field import FieldMapping, Ignore

logger = logging.getLogger("datamapping.source")
_mapping_registry = {}

__all__ = [
    "locate",
    "maps",
    "SourceMapping",
    "ListMapper",
    "ValueMapper"
]


def locate(mapped_source):
    source = ascls(mapped_source)
    try:
        return _mapping_registry[source]
    except KeyError:
        for k, mapping in _mapping_registry.items():
            if issubclass(source, k):
                # Cache the result so looping isn't repeated
                _mapping_registry[source] = mapping
                return mapping
    raise NotImplementedError(f"Mapping for {mapped_source} not found")


t_From = TypeVar("t_From")


def _normalize_input(frmorto=None, /, to=None, frm=None):
    to = to or frmorto
    frm = (frmorto if to != frmorto else None) or frm
    return frm, to


class GetValueError(Exception):
    pass


def maps(frmorto: Type[t_From] = None, /, to: Union[Type, List[Type]] = None,
         via: Callable[[t_From], Text] = None, frm=None, intermediary=None):
    """A declarative decorator to help specify the relationship a mapping has between Data collections and
    Data sources.

    :param frmorto: In the absence of a second parameter frmorto is To. Otherwise it is frm (from)
    :param to: The expected resulting type(s) of the mapping. Mappings are not bound to this type and can produce
     different types. Mapping uses this type tp provide the initial context for where the mapped values are set.

    :param via: The type expected depends on the class being decorated. When decorating a ValueMapper
     via expected to be a callable where the frm is passed and the return is the value being mapped. In
     all other cases the type of via is expected to be a ValueMapper that will be used.

    :param frm: Meaning from, represents the source of the data. Generally this is irrelevant and does not need
     inclusion. A source of data *is* important ValueMappers as the mapping that references the ValueMapping
     using via will defer to the frm of the ValueMapper as the context for it's mapping

     :param intermediary: When mapping via a ValueMapper the mappings must have  an intermediary. This
     intermediary is technically the real "to" of the decorated mapping. If it is not specified the frm
     of the ValueMapper is used as the Intermediary, if the frm is not defined on the value mapper, a
     dictionary is used.

    :return:
    """

    frm, to = _normalize_input(frmorto, to=to, frm=frm)
    data_collection = None
    for to_i in [to] if not isinstance(to, List) else to:
        mappable_to = mappable(to_i)
        data_collection = data_collection or mappable_to
    sub_mapping = None
    if via and isinstance(via, MappingType) and issubclass(via, ValueMapper):
        data_collection = mappable(intermediary) or mappable(via.source_collection)
        sub_mapping = via

    def wrapper(mapping_cls):
        global _mapping_registry

        mapping_cls.source_collection = frm
        mapping_cls.sub_mapping = sub_mapping

        def boundable_via(s, i):
            try:
                return via(i)
            except Exception:
                raise GetValueError()

        if issubclass(mapping_cls, ValueMapper):
            mapping_cls.get_value = boundable_via

        if data_collection:
            mapping_cls.target_collection = data_collection
        else:
            mapping_cls.target_collection = None
        return mapping_cls

    return wrapper


class Strictness(object):
    PRIORITY = 1
    STRICT = 2
    LOOSE = 3


def to_list(thing):
    return [thing] if not isinstance(thing, list) else thing


class MappingType(type):

    def __new__(mcs, name, bases, members):
        # Note that we replace the classdict with a regular
        # dict before passing it to the superclass, so that we
        # don't continue to record member names after the class
        # has been created.
        kls = type.__new__(mcs, name, bases, dict(members))
        fms = {}
        for k, v in members.items():
            for fm in to_list(v):
                if isinstance(fm, FieldMapping):
                    mcs.add_field_mapping(fm, k, fms)
        for base in bases:
            for k, v in getattr(base, "_field_mappings", dict()).items():
                if k not in fms:
                    for fm in v:
                        mcs.add_field_mapping(fm, k, fms)

        kls._field_mappings = fms
        return dataclass(kls)

    @staticmethod
    def add_field_mapping(mapping, key, fields):
        if mapping.path is None:
            mapping.path = key
        heading = mapping.tokenized_path[0]
        fields.setdefault(heading, [])
        fields[heading].append(mapping)


T_item = TypeVar("T_item")


def proxy(func):
    @wraps(func)
    def wrapper(s, other):
        if isinstance(other, AnnotatedValue):
            other = other.value
        try:
            return func(type(other)(s.value), other)
        except BaseException as ex:
            tb = ex.__traceback__
            raise ex.with_traceback(AnnotatedValue.remove_frame(tb, 1)) from None

    return wrapper


class AnnotatedValue(object):
    value: Any

    def __init__(self, v):
        self.value = v

    def __instancecheck__(self, instance):
        return self.value.__instancecheck__(instance)

    def __getattr__(self, item):
        v = proxy(self.value.__getattribute__(item))
        return v

    def __setattr__(self, key, value):
        if key != "value":
            key = "@" + key.lstrip("@")
        super().__setattr__(key, value)

    def __eq__(self, other):
        return self.value.__eq__(other)

    @proxy
    def __add__(self, other):
        return self.__add__(other)

    def __iadd__(self, other):
        return self.value.__iadd__(other)

    @proxy
    def __radd__(self, other):
        return self.__radd__(other)
        # return self.value.__radd__(other)

    def __hash__(self):
        return self.value.__hash__()

    def __sub__(self, other):
        if isinstance(other, AnnotatedValue):
            other = other.value
        try:
            return self.value.__sub__(other)
        except AttributeError as ex:
            tb = ex.__traceback__

            try:
                self.value - other
            except Exception as iex:
                raise iex.with_traceback(self.remove_frame(tb, 1)) from None

    def __neg__(self):
        return self.value.__neg__()

    def __repr__(self):
        return self.value.__repr__()

    def __str__(self):
        return self.value.__str__()

    @staticmethod
    def remove_frame(tb, times):
        frame = tb.tb_frame
        for _ in range(times):
            frame = frame.f_back
        return type(tb)(None, frame, frame.f_lasti, frame.f_lineno)


class SourceMapping(object, metaclass=MappingType):
    target_collection: Type = field(init=False, default=None)
    root: Text = field(default=None)
    should_annotate: bool = field(default=False)

    _parent: 'SourceMapping' = field(init=False, default=None, repr=False)
    _item_cache: dict = field(init=False, default_factory=dict, repr=False)
    _mapping_context: object = field(init=False, default=None, repr=False)

    def create_mapping_context(self, init_data):
        """In some cases the Mapping may be to a generic object like Employee but based off some data, or combination
        of data,  the object created, and thus mapped into, might be a FTE or a Contractor. In these cases
        create_mapping_context can be overridden to provide logic. A more expressive route may be to use
        ValueMappers.

        :param init_data:
        :return:
        """

        if self.target_collection:
            self._mapping_context = self.target_collection(**init_data)
            return self._mapping_context, {}
        else:
            if self._parent:
                item = self._parent.get_item()
            else:
                item = None
            return item, init_data

    @property
    def path(self):
        if self._parent:
            prefix = self._parent.path
            if len(prefix):
                prefix += "."
            path = f"{prefix}{self.root}"
        else:
            path = self.root or ""
        return path

    @property
    def annotate(self):
        if self._parent:
            return self._parent.annotate
        else:
            return self.should_annotate

    @property
    def store_unmapped(self) -> bool:
        """As a row is mapped any key/heading that is not mapped will be stored or discarded based off the value
        returned by this.

        :return: list of unicode
        """
        return True

    def unmapped_data(self, values):
        if self.store_unmapped:
            return values
        else:
            return {}

    def get_mappings(self, heading: Text) -> List[FieldMapping]:
        try:
            field_mappings = self._field_mappings.get(heading, self._field_mappings.get(heading.lower(), []))
        except AttributeError:
            return []
        for fm in field_mappings:
            fm.mapper = self
        return field_mappings

    def mapping_complete(self, item=None):
        """In some cases a field cannot be cleaned when it is set, generally if the value is dependent on some other
        value in the activity. In these cases they should be set/fixed in this method.

        :param item: A newly created, not yet saved :class:`.StudentActivity`.
        :type item: :class:`~data_wrangler.activity.StudentActivity`
        """
        return item

    @classmethod
    def each(cls, item):
        try:
            item.save()
        except Exception as original_ex:
            logger.exception(original_ex)
            logger.error(item.__class__.__name__)
            raise

    # ignored = IgnoreProperty

    @staticmethod
    def Ignore():
        return lambda self, doc, key, value: ''

    @staticmethod
    def MapTo(field=None, converter=lambda value, key: value, path=None):
        pass

    def before_mapping(self):
        pass

    def map_item(self, raw_data, headings=None, mapping_complete: Callable = None):
        self.before_mapping()
        mapping_complete = mapping_complete or self.mapping_complete

        self.supplement_data(raw_data)
        if isinstance(raw_data, (list, tuple)):
            raw_data = zip(headings or [], raw_data)
        elif is_dataclass(raw_data):
            raw_data = [(f.name, getattr(raw_data, f.name)) for f in fields(raw_data)]
        else:
            raw_data = raw_data.items()
        unmapped_data = {}
        # self.current_data = dict(raw_data)
        # self.mapping_item = self.item
        init_data = {}
        post_init = list()
        for header, value in raw_data:
            # value = self.normalize_value(value)
            field_mappings = self.get_mappings(header)
            if field_mappings is None or len(field_mappings) == 0:
                unmapped_data[header] = value
            else:
                for field_mapping in field_mappings:
                    if isinstance(field_mapping, Ignore):
                        continue
                    raw_value = field_mapping.extract_value(value)
                    try:
                        converted_value = self.convert_value(field_mapping, header, raw_value)
                    except IgnoreEntry:
                        continue

                    if isinstance(field_mapping.converter, SourceMapping):
                        self.cache_item(converted_value)
                    if field_mapping.init_data:
                        init_data[field_mapping.key] = converted_value
                    else:
                        post_init.append((field_mapping, converted_value))

        simple_set = self.unmapped_data(unmapped_data)

        item, unset_values = self.create_mapping_context(init_data)
        logger.debug(f" {type(self).__name__} Context: {ascls(item).__name__}, Unset Values: {len(unset_values) > 0}")
        if item is None:
            logger.info("No context found, will create a default context")
            item = self.create_default_context()
        simple_set.update(unset_values)

        for fm, value in post_init:
            self.defer_to_field_mapping(fm, item, value)
        if item is not None:
            for k, v in simple_set.items():
                setattr(item, str(k), v)
        else:
            if len(simple_set):
                logger.warning("Mapping context is none (which means something overrode create_default_context) the "
                               f"following values will not be set and thus, most likely lost:\n{simple_set}")

        outcome = mapping_complete(item=item) or item
        for item in [outcome] if not isinstance(outcome, list) else outcome:
            self.cache_item(item)
        del item

        if self.sub_mapping:
            sub_outcomes = []
            mapping_instance = self.sub_mapping(root=self.root)
            mapping_instance.add_parent(self)
            for item in [outcome] if not isinstance(outcome, list) else outcome:
                sub_outcome = mapping_instance.map_item(item)
                sub_outcome = [sub_outcome] if not isinstance(sub_outcome, list) else sub_outcome
                sub_outcomes.extend(sub_outcome)
            if len(sub_outcomes) == 1 and not isinstance(outcome, list):
                outcome = sub_outcomes[0]
            else:
                outcome = sub_outcomes

        return outcome

    def add_parent(self, parent):
        self._parent = parent

    def cache_item(self, item):
        try:
            self._parent._item_cache[type(item)] = item
        except AttributeError:
            pass
        return item

    def get_item(self, item_cls=None, strict=Strictness.LOOSE, ancestors: int = None):
        def strict_compare(obj, cls):
            return ascls(obj) == cls

        if (item_cls is None
                or strict_compare(self._mapping_context, item_cls)
                or (strict == Strictness.LOOSE
                    and self.instanceof(self._mapping_context, item_cls))):
            return self._mapping_context

        loose_match = None
        for k, item in self._item_cache.items():
            if strict_compare(k, item_cls):
                return item
            if strict == Strictness.LOOSE and self.instanceof(item, item_cls):
                loose_match = item
                strict = Strictness.STRICT
        if loose_match:
            return loose_match

        if self._parent is not None and (ancestors or 1) >= 1:
            if ancestors is not None:
                ancestors = ancestors - 1

            item = self._parent.get_item(item_cls, strict=Strictness.STRICT, ancestors=0)
            if item is _Empty:
                item = self._parent.get_item(item_cls, strict=strict, ancestors=ancestors)
            return item

        return _Empty

    @staticmethod
    def instanceof(obj, kls):
        if not is_generic_type(kls):
            return isinstance(obj, kls)

        obj_tp = get_generic_type(obj)
        templates = list(get_bound(t) or t.__constraints__ for t in get_parameters(get_origin(kls)))

        try:
            if issubclass(type(obj), get_origin(kls) or kls):
                for bound, obj_sub_tp in zip(templates, get_args(obj_tp)):

                    if not issubclass(obj_sub_tp, bound):
                        return False
                return True
        except TypeError as ex:
            raise ex

    def annotated(self, v, field_mapping, field_converter):
        if self.annotate and isinstance(v, (str, int, DateTime, float)):
            v = AnnotatedValue(v)
            prefix = self.root
            if len(prefix):
                prefix += "."
            v.path = f"{prefix}{field_mapping.path}"

            if field_converter is not None and not isinstance(field_converter, SourceMapping):
                v.MethodExecuted = field_converter.__name__
                v.MethodDocstring = (field_converter.__doc__ or "").lstrip().rstrip().split("\n")
        return v

    @staticmethod
    def normalize_value(value):
        if not isinstance(value, (int, float, DateTime, str, dict, list)):
            try:
                # CSV wants it all encoded into UTF 8 so we must decode out of UTF8
                value = value.decode("utf-8")
                # We live in a windows world and lots of things are not in any sort of useful thing try this if UTF fails.
                value = value.decode("Windows-1252")
            except Exception as ex:
                try:
                    value = str(value)
                except Exception as ex2:
                    raise ex2 from ex
        return value

    def convert_value(self, field_mapping, header, raw_value):
        field_converter = field_mapping.converter
        if isinstance(field_converter, SourceMapping):
            prefix = self.path
            if len(prefix):
                prefix += "."
            field_converter.root = f"{prefix}{header}"
            field_converter.add_parent(self)

        try:
            value = field_mapping.convert(raw_value)
        except IgnoreEntry:
            raise
        except Exception as ex:
            msg = f"Error while converting '{header}' to the mappable value using {field_converter}.\n" \
                  f"{json.dumps(raw_value, indent=' ')} "
            raise MappingError(msg) from ex
        return self.annotated(value, field_mapping, field_converter)

    def defer_to_field_mapping(self, fm, item, value):
        return fm.call_target(item, value)

    def create_default_context(self):
        self._mapping_context = MappingScope()
        return self._mapping_context

    def supplement_data(self, raw_data):
        pass


class MappingScope(object):

    def __init__(self):
        self._keys = set()

    def __setattr__(self, item, v):
        try:
            self._keys.add(item)
        except AttributeError:
            pass
        return super().__setattr__(item, v)

    def items(self):
        for k in self._keys:
            yield (k, self.__dict__[k])


class IgnoreEntry(Exception):
    pass


class ValueMapper(SourceMapping):
    """Sometimes the value (or combination of values) will detirmine how the mapping should work,
    for example maybe the Source system has a status flag of active, retired, or terminated  and
    based on that the Canonical
    model might have an Employee, RetiredAssociate or  nothing as this system does not care about
     terminated employees. The value Mapper can be used to express
    that sort of simple decision.
    .. code-block::python

        @maps(IntermediaryEmployee, to=[ActiveEmployee,RetiredEmployee], via=lambda e: e.status)
        class EmployeeMapper(ValueMapper)
            active = FieldMapping(ActiveEmployeeMapping)
            retired = FieldMapping(RetriedEmployeeMapping)
            terminated = Ignored()
            _other = Ignored()
    """
    _other = None
    _mapped_items: List = field(init=False, default_factory=list)

    def create_mapping_context(self, init_data):
        """The typical usecase for a value mapper is to map a value to different mappings. As a result the
        default / expected behavior of this mapper is to pass through the outcomes of the field (value in this case)
        mappings. """
        return self, init_data

    def defer_to_field_mapping(self, fm, item, value):
        if fm.target is None:
            self._mapped_items.append(value)
            return True
        return super(ValueMapper, self).defer_to_field_mapping(fm, item, value)

    def get_mappings(self, heading: Text) -> List[FieldMapping]:
        m = super().get_mappings(heading)
        if m in (None, []):
            m = [type(self)._other or Ignore()]
        return m

    def map_item(self, raw_data, headings=None, mapping_complete: Callable = None):
        def wrap_complete(item):
            o = mapping_complete(item)
            if o == self:
                return None
            return o

        pivot_data = {str(self.get_value(raw_data)): raw_data}
        outcome = super().map_item(pivot_data, wrap_complete)
        if outcome == self:
            return self._mapped_items
        return outcome


class ListMapper(SourceMapping):
    """In some special cases when dealing with unstructured data a List might need to be mapped not as a list but
    as individual items. This mapper is for those scenarios.

    """

    def handle_non_list(self, data, headings):
        logger.warning(f"List Mapping provided but the value provided is not a list {data}")
        return super().map_item(data, headings)

    def item_mapping_complete(self, item=None):
        """In some cases a field cannot be cleaned when it is set, generally if the value is dependent on some other
        value in the activity. In these cases they should be set/fixed in this method.

        :param item: A newly created, not yet saved :class:`.StudentActivity`.
        :type item: :class:`~data_wrangler.activity.StudentActivity`
        """
        return item

    def map_item(self, raw_data, headings=None, mapping_complete=None):
        items = []
        mapping_complete = mapping_complete or self.mapping_complete
        if not isinstance(raw_data, list):
            self.handle_non_list(raw_data, headings)
        for idx, item in enumerate(raw_data):
            try:
                value = super().map_item(item, headings, self.item_mapping_complete)
            except IgnoreEntry:
                continue
            else:
                if isinstance(value, list):
                    items.extend(value)
                else:
                    items.append(value)

        return mapping_complete(items)
